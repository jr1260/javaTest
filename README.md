***本文档由 Rap2 (https://github.com/thx/rap2-delos) 生成***

***本项目仓库：[http://rap2.taobao.org/repository/editor?id=177015](http://rap2.taobao.org/repository/editor?id=177015) ***

# 仓库：原单汇商城
## 模块：示例模块
### 接口：test
* 地址：https://www.baidu.com
* 类型：GET
* 状态码：200
* 简介：测试
* Rap地址：[http://rap2.taobao.org/repository/editor?id=177015&mod=260076&itf=1321802](http://rap2.taobao.org/repository/editor?id=177015&mod=260076&itf=1321802)
* 请求接口格式：

```

```

* 返回接口格式：

```

```


### 接口：test2
* 地址：http://211.149.225.76:18088/scenery_cost/api/token/tokenVerify
* 类型：GET
* 状态码：200
* 简介：无
* Rap地址：[http://rap2.taobao.org/repository/editor?id=177015&mod=260076&itf=1321816](http://rap2.taobao.org/repository/editor?id=177015&mod=260076&itf=1321816)
* 请求接口格式：

```

```

* 返回接口格式：

```

```


## 模块：app
### 接口：测试
* 地址：https://www.wr.net/app/klinevtwo/tradingview?langCode=zh_CN&symbol=BCH_USDT
* 类型：GET
* 状态码：200
* 简介：无
* Rap地址：[http://rap2.taobao.org/repository/editor?id=177015&mod=260077&itf=956218](http://rap2.taobao.org/repository/editor?id=177015&mod=260077&itf=956218)
* 请求接口格式：

```

```

* 返回接口格式：

```

```


### 接口：test
* 地址：https://www.wr.net/app/klinevtwo/tradingview
* 类型：GET
* 状态码：200
* 简介：无
* Rap地址：[http://rap2.taobao.org/repository/editor?id=177015&mod=260077&itf=956219](http://rap2.taobao.org/repository/editor?id=177015&mod=260077&itf=956219)
* 请求接口格式：

```
├─ langCode: String 
└─ symbol: String 

```

* 返回接口格式：

```

```


## 模块：后台